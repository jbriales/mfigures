function multiboxplot( statFile, cfgFile, yparam )

% Axis are:
%   s - series
%   x - horizontal
%   y - vertical

if isa(statFile,'Stat')
  s = statFile;
else
  % Load .mat file with stored results
  try
    load( statFile )
  catch
    warning('Unable to read file %s. Check synchronization to Peregrine',statFile);
    return
  end
end
% convert data into Stat class
s = Stat(s);

% read parameters
if isa(cfgFile,'struct')
  cfg = cfgFile;
else
  cfg = yaml.ReadYaml(cfgFile);
end

% assert( ischar(params.y), 'Only one field can be plotted in Y axis' )
% TODO: Read statistical metric(s) to use from parameters, with str2func
% statFun = @mean;
% statFun = @median;
% statFun = @max;
statFun = @(x,dim)prctile(x,75,dim);

%% set plot parameters
fgrCfg = cfg.fgr;
% complete bundle of evaluated parameters
tParams = Parameterization(cfg.eval.layers);

% bundle of parameters in axis X
params_x = tParams.subtree(fgrCfg.x.id);
vec_x = cell2mat( forceCell( tParams.get(fgrCfg.x.id) ) ); % Vector of X values
labels_x = arrayfun(@num2str,vec_x,'UniformOutput',false);

% different values in axis Y
% if nargin < 3
  params_y = fgrCfg.y;
% else
%   params_y = yparam;
% end
params_y = forceCell(params_y); % make cell even if scalar element

% bundle of parameters in different series
params_s = tParams.subtree(fgrCfg.s.id);

% unbundle parameters
arrParams_s = unroll_struct(params_s);
arrParams_x = unroll_struct(params_x);
% TODO: x-axis should span a single parameter dimension, assert

for figIdx = 1:numel(params_y)
  
  param_y = params_y{figIdx};
  
  figure, hold on
  
  % Traverse all combinations of parameters in s-axis
  data = cell(numel(arrParams_s),numel(arrParams_x));
  labels_s = cell(1,numel(arrParams_s));
  for i=1:numel(arrParams_s) % LOOP: Series
    % Traverse vector of values in current s-point
    Y = [];
    for k=1:numel(arrParams_x) % LOOP: X
      
      % Compose complete parameterization
      % compose( constant_part_of_parameterization, ...
      %          fgrCfg.s.id, arrParams_s(i), ...
      %          fgrCfg.x.id, arrParams_x(k) );
      
      %     p = mergeOptions(arrParams_s(i),arrParams_x(k));
      % TODO: Ensure the merge occures at nested levels too
      p = mergeOptions(arrParams_s(i),arrParams_x(k));
      % find desired entry in the stored results
      curr_s = find(s,p);
      
      %     % check there is a unique point
      %     assert( numel(curr_s)==1 )
      % concatenate all the results into a single array
      out = [curr_s.out];
      % TODO: For now, this is the quickest shorcut:
      %       Extract array of each subfield
      results = [out.results];
      times   = [out.times];
      assert( isvector(results) );
      % add new column to data matrix (rows - samples, columns - xpoints)
      if isfield(param_y,'id')
        values = [results.(param_y.id)];
      elseif isfield(param_y,'fun')
        values = eval( param_y.fun );
      elseif ischar(param_y)
        values = [results.(param_y)];
      else
        error('Wrong id to plot in Y')
      end
%       data{i,k} = values(:);
      % TEMPORAL:
      % Make sure the values are zero or positive
      values(values<0) = 0;
      data{i,k} = values;
      
      % collect results about tightness
      % TODO: This is specific for current use, should be refactored
      %       And not repeat! (same info for any method here)
      vTight(k) = mean( [results.tight] );
      
    end
       
    % add current line to legend
    labels_s{i} = params2legend( arrParams_s(i), fgrCfg.s.id );
  end
  
  multiple_boxplot(data',labels_x,labels_s)
  % Fix minimum value in Y axis (gap is always >=0)
  ylimmin(0)  
    
  xlabel(fgrCfg.x.label)
  if isfield(param_y,'label')
    ylabel(param_y.label)
  else
    ylabel(param_y.id)
  end
  
  % get position of X ticks for groups
  xticks = get(gca,'xtick');
  % plot line with tightness trend
  yyaxis right
  plot(xticks,(1-vTight)*100,'vk','MarkerFaceColor','k')
  ylim([0 100])
  % add tags with percentage of tight cases
%   text(xticks,ylimmax*ones(1,numel(xticks)), num2str(100*vTight'))
  ylabel('% non-tight instances')
  
  % all the groups should have the same number of samples
  num_samples = unique( cellfun(@numel,data) );
  if numel(num_samples) == 1
    title(sprintf('%s, #samples=%d',fgrCfg.title,num_samples))
  else
    warning('Different number of samples')
    disp(num_samples)
    title(fgrCfg.title)
  end
  
%   keyboard
  
end

end

function str = params2legend( param_s, id )

fields = getCellOfFields( id );
p = getfield( param_s, fields{:} );

if ~isstruct(p)
  str = p;
else
  
  switch id
    case 'solver'
      opts = p.opts;
      switch p.method
        case 'linear'
          str = ['lin-',opts.formulation,'-',opts.constraint];
        case 'via_dual'
          str = ['lin-init(',opts.metricUpgInit,')-ref(',opts.metricUpgRef,')'];
      end
    otherwise
      error('Unset id %s',id);
  end
  
end

end