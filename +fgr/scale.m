function [c_scaledData,tickFun] = scale(type,c_data)

switch type
  case '1+x'
    scalefun = @(x)1+x;
    c_scaledData = nonunifcellfun( scalefun, c_data );
    
    tickFun = @undoOffset;
      
  case 'linear'
    c_scaledData = c_data;
    
    tickValues = [];
    tickLabels = [];
  case 'log10(1+x)'
    % set positive data only
    for ii=1:numel(c_data)
      if any(x<0)
        warning('Negative data found!')
      end
      x = c_data{ii};
      x(x<0)=0;
      c_data{ii} = x;
    end
    
    scalefun = @(x)1+x;
    c_scaledData = nonunifcellfun( scalefun, c_data );
    
    tickValues = [];
    tickLabels = [];
    
  case 'old_log10(1+x)'
    scalefun = @(x)log10(1+x);
    c_scaledData = nonunifcellfun( scalefun, c_data );
    
    %% get tick values
    % get minimum and maximum data
    mins = cellfun(@min,c_data);
    maxs = cellfun(@max,c_data);
    span = [min(mins(:)),max(maxs(:))];
    if span(1)<0
      warning('There should not be negative values in log10(1+x) scale')
      span(1) = 0;
    end
    % get ticks (according to number of ticks)
    [b,E] = fp_decomp( span(2) );
    nticks = 5;
    [b,e] = fp_decomp( E/(nticks-1) );
    bases = [1 2 5 10];
    [~,idx] = min( abs(bases-b) );
    if idx==4
      warning('TODO: If closest base is 10')
    end
    step = fp_comp(bases(idx),e);
    
    % get concrete tick values to show in the figure
    exponents = 0:step:E;
    tvals = [0, 10.^exponents];
    tickValues = scalefun(tvals);
    tickLabels = ['0', num2labels(exponents,'10^%d')];
    
%     r = get(gca,'YRuler');
%     r.TickValues = tickValues;
%     r.TickLabels = tickLabels;
        
%     keyboard
  otherwise
    error('Unknown scale type %s',type);
end

end

% STUFF: for sqrt scale?
% get ticks (according to number of ticks)
%     nticks = 5;
%     [b,e] = fp_decomp( diff(span)/(nticks-1) );
%     bases = [1 2 5 10];
%     [~,idx] = min( abs(bases-b) );
%     step = fp_comp(bases(idx),e);
%     nticks = round( diff(origBreaks(1:2))/step+1 );
%     if idx==4
%     

function undoOffset(r)
% undoOffset(RULER)
% Inverse function for scale with mode '1+x'

switch r.Scale
  case 'linear'
    % TODO: Analyze behaviour of ticks to customize
    error('Linear case not set yet');
  case 'log'
    % preserve nice exp notation
    % TODO: Set ruler instead of gca?
    set(gca,'TickLabelInterpreter','tex')
    
    % get tick values
    tvals = r.TickValues;
    %     ylimmax(ylimmax()+1) % Put ylim upper
    
    % In the log case, behaviour depends on how big values are
    if all(tvals<10)
      % The ticks follow a mainly linear fashion,
      % keep same ticks and fix the labels to the true mapped value
      trueValues = tvals-1; % TODO: Use inverse function in general?
      r.TickLabels = num2labels(trueValues);
    else
      % The ticks follow a log fashion,
      % move the ticks ahead the offset to get true 10^d positions
      % and insert tick at one (to have true 0 tick)
      r.TickValues = [1, tvals+1];
      % add 0 label for the preprended point (the rest keep the same val)
      r.TickLabels = ['0', num2labels(log10(tvals),'10^{%d}')];
      % move also the minor ticks
      mticks = setdiff(r.MinorTickValues,2:9);
      % prepend 8 minor ticks between 1 and 2 and between 2 and 10
      r.MinorTickValues = [linspace(1.1,1.9,8),linspace(3,9,8),mticks];
    end
    % To avoid strange effects after customization, remove minor tick
%     r.MinorTick = 'off';
%     set(r,'MinorTickValues',[1.1:0.1:1.9,linspace(3,9,8),linspace(20,90,8)])
   
  otherwise
    error('Unknown scale %s',r.Scale);
end

end