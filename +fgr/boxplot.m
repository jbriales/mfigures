function boxplot( statFile, yamlFile )

% Axis are:
%   s - series
%   x - horizontal
%   y - vertical

if isa(statFile,'Stat')
  s = statFile;
else
  % Load .mat file with stored results
  load( statFile )
end

% read parameters
if isa(yamlFile,'struct')
  params = yamlFile;
else
  params = yaml.ReadYaml(yamlFile);
end

% TODO: assert there are all necessary fields
assert( ischar(params.y), 'Only one field can be plotted in Y axis' )

% Get list of instances to be represented
[u_params.x,unrolled_labels] = unroll_struct(params.x);

% Preallocate data matrix
% TODO
% Traverse all combinations of parameters in x-axis
for i=1:numel(u_params.x)
  % find desired entry in the stored results
  curr_params = u_params.x(i);
  curr_s = find(s,curr_params);
  
  % concatenate all the results into a single array
  results = [curr_s.out];
  assert( isvector(results) );
  
  % add new column to data matrix for boxplot
  X(:,i) = [results.(params.y)];
  
  % add corresponding group tags to current column in the matrix
  [tags,vals] = plain_struct(curr_params);
%   [tags;vals]'
  col_groups{i} = vals;
end

% create naive boxplot
% boxplot(X)

% Analyze grouping variables and complete
% get max number of elements (groups) for any column
n_groups = max(cellfun(@numel,col_groups));
assert(numel(col_groups)==size(X,2))
for i=1:n_groups
  for j=1:numel(col_groups)
    try
      G{i}{j} = col_groups{j}{i};
    catch
      G{i}{j} = 'foo';
    end
  end
end

% remove dummy groups
doRemove = false(1,n_groups);
for i=1:n_groups
  doRemove(i) = numel(unique(G{i}))==1;
end
G(doRemove) = [];

% for i=1:numel(G)
%   if numel(G{i})<n
%     G{i}(end+1:n) = {'def'};
%   end
% end
% % transpose
% G = G';

% % Group according to unrolled labels
% % Create grouping variables
% % - all X columns sharing a same group label go to the same box.
% % - if there are several grouping criteria, all X columns sharing
% %   ALL the same labels go to the same box
% % - if there are not 2 columns with exactly the same labels,
% %   this is equivalent to no-grouping (but still produces nice labels!)
% if isfield(params,'groups')
%   all = [u_params.x.solver];
%   for i=1:numel(params.groups)
%     G{i} = {all.(params.groups{i})};
%   end
%   % TODO: In HTML, group dynamically from user controller
%   
% else
%   g = struct2cell( [u_params.x.solver] ); % TODO: not only solver!
%   % remove singleton dimensions
%   g = squeeze(g);
%   for i=1:size(g,1)
%     G{i} = {g{i,:}};
%   end
%   
% end

% create boxplot with boxs tagged according to several groups
boxplot(X,G);

% boxplot(X,'Labels',params.x.solver.metricUpg)

% add legend to figure
% legend(labels{:},'Location','SouthEast')

% xlabel(params.labels.x)
ylabel(params.y) % TODO: Use labels

title(params.title)

end