function plot( statFile, cfgFile )

% Axis are:
%   s - series
%   x - horizontal
%   y - vertical

if isa(statFile,'Stat')
  s = statFile;
else
  % Load .mat file with stored results
  load( statFile )
end
% convert data into Stat class
s = Stat(s);

% read parameters
if isa(cfgFile,'struct')
  cfg = cfgFile;
else
  cfg = yaml.ReadYaml(cfgFile);
end

% assert( ischar(params.y), 'Only one field can be plotted in Y axis' )
% TODO: Read statistical metric(s) to use from parameters, with str2func
% statFun = @mean;
% statFun = @median;
% statFun = @max;
statFun = @(x,dim)prctile(x,75,dim);

%% set plot parameters
fgrCfg = cfg.fgr;
% complete bundle of evaluated parameters
tParams = Parameterization(cfg.eval.layers);

% bundle of parameters in axis X
params_x = tParams.subtree(fgrCfg.x.id);
vec_x = cell2mat( tParams.get(fgrCfg.x.id) ); % Vector of X values

% different values in axis Y
params_y = fgrCfg.y;
params_y = forceCell(params_y); % make cell even if scalar element

% bundle of parameters in different series
params_s = tParams.subtree(fgrCfg.s.id);

% unbundle parameters
arrParams_s = unroll_struct(params_s);
arrParams_x = unroll_struct(params_x);
% TODO: x-axis should span a single parameter dimension, assert

for figIdx = 1:numel(params_y)
  
  param_y = params_y{figIdx};
  
  figure, hold on
  
  % Traverse all combinations of parameters in s-axis
  labels_s = cell(1,numel(arrParams_s));
  for i=1:numel(arrParams_s) % LOOP: Series
    % Traverse vector of values in current s-point
    Y = [];
    for k=1:numel(arrParams_x) % LOOP: X
      
      % Compose complete parameterization
      % compose( constant_part_of_parameterization, ...
      %          fgrCfg.s.id, arrParams_s(i), ...
      %          fgrCfg.x.id, arrParams_x(k) );
      
      %     p = mergeOptions(arrParams_s(i),arrParams_x(k));
      % TODO: Ensure the merge occures at nested levels too
      p = mergeOptions(arrParams_s(i),arrParams_x(k));
      % find desired entry in the stored results
      curr_s = find(s,p);
      
      %     % check there is a unique point
      %     assert( numel(curr_s)==1 )
      % concatenate all the results into a single array
      results = [curr_s.out];
      assert( isvector(results) );
      % add new column to data matrix (rows - samples, columns - xpoints)
      if isfield(param_y,'id')
        Y(:,k) = [results.(param_y.id)];
      elseif isfield(param_y,'fun')
        Y(:,k) = eval( param_y.fun );
      else
        error('Wrong id to plot in Y')
      end
      
    end
    
    % apply metric to current array of results
    vec_y = statFun( Y, 1 );
    %   v(k) = Stat.metric( results, yparam, statFun );
    
    % plot curve for current s-point
    plot(vec_x, vec_y,'LineWidth',2);
    
    % add current line to legend
    labels_s{i} = params2legend( arrParams_s(i), fgrCfg.s.id );
  end
  
  % add legend to figure
  legend(labels_s{:},'Location','SouthEast')
  
  if isfield(fgrCfg.x,'label')
    xlabel(fgrCfg.x.label)
  else
    xlabel(fgrCfg.x.id)
  end
  if isfield(param_y,'label')
    ylabel(param_y.label)
  else
    ylabel(param_y.id)
  end
  
  title(fgrCfg.title)
  
%   keyboard
  
end

end

function str = params2legend( param_s, id )

fields = getCellOfFields( id );
p = getfield( param_s, fields{:} );

if ~isstruct(p)
  str = p;
else
  
  switch id
    case 'solver'
      opts = p.opts;
      switch p.method
        case 'linear'
          str = ['lin-',opts.formulation,'-',opts.constraint];
        case 'via_dual'
          str = ['lin-init(',opts.metricUpgInit,')-ref(',opts.metricUpgRef,')'];
      end
    otherwise
      error('Unset id %s',id);
  end
  
end

end