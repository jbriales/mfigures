function value = fp_comp(b,e)
% value = fp_comp(n_man,n_exp)
% Get floating point number as double from mantissa and exponent

% rebuild the floating point number
value = b.*10.^e;

end