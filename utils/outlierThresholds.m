function thres = outlierThresholds( v )
% thresholds = outlierThresholds( v )
% Compute the thresholds to test outliers in a set of values,
% using Tukey's test: inliers are in the range [Q1-k*IQR,Q3+k*IQR],
% where k=1.5 and IQR=Q3-Q1. See Wikipedia.

IQR = iqr(v);
k   = 1.5;
thres = [ prctile(v,25) - k*IQR, prctile(v,75) + k*IQR ];