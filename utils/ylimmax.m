function a = ylimmax(arg)
%YLIMMMAX Y maximum limit.

if nargin == 0
  a = ylim;
  a = a(2);
else
  v = ylim;
  v(2) = arg;
  ylim(v)
end