function setFig_square(hF)
%setFig_square(hF)
% Make figure square in size

position = get(hF,'position');
average_size = mean(position(3:4));

% Set new position in same location
% but with average size in height and width
new_position = position;
new_position(3:4) = average_size;

set(hF,'position',new_position)