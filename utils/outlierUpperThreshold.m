function thres = outlierUpperThreshold( v )
% upperThres = outlierUpperThreshold( v )
% Compute the upper threshold to test outliers in a set of values,
% using Tukey's test: upper outliers are above Q3+k*IQR,
% where k=1.5 and IQR=Q3-Q1. See Wikipedia.

thres = outlierThresholds( v );
thres = thres(2);