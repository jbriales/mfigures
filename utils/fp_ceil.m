function value = fp_ceil( value )
% apply ceil rounding towards the closest non-zero magnitude
%
% See alo fp_decomp.

% get mantissa and exponent
[b,e] = fp_decomp(value);

% ceil the mantissa
b = ceil(b);

% rebuild the floating point number
value = b.*10.^e;