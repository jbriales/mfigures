function [n_man,n_exp] = fp_decomp(value)
% [n_man,n_exp] = fp_decomp(value)
% Get mantissa and exponent for floating point number

% get order of the number in base 10
n_exp = floor( log10(abs(value)) );
% get mantissa (with sign)
n_man = value./(10.^n_exp);

end