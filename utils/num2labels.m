function c_labels = num2labels( v, varargin )
% c_labels = num2labels( v )
% Convert vector of numbers into cell array of string tags,
% e.g. for TickLabels in axes.
%
% See also ...

c_labels = arrayfun(@(x)num2str(x,varargin{:}),v, 'UniformOutput',false);