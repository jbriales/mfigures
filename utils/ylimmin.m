function a = ylimmin(arg)
%YLIMMIN Y minimum limit.

if nargin == 0
  a = ylim;
  a = a(1);
else
  v = ylim;
  v(1) = arg;
  ylim(v)
end