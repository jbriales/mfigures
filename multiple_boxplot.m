function multiple_boxplot(data,xlab,Mlab,colors)

% data is a cell matrix of MxL where in each element there is a array of N
% length. M is how many data for the same group, L, how many groups.
%
% Optional:
% xlab is a cell array of strings of length L with the names of each
% group
%
% Mlab is a cell array of strings of length M
%
% colors is a Mx4 matrix with normalized RGBA colors for each M.

% check that data is ok.
if ~iscell(data)
    error('Input data is not even a cell array!');
end

% Get sizes
numS=size(data,2);
numX=size(data,1);
if nargin>=4
    if size(colors,2)~=numS
        error('Wrong amount of colors!');
    end
end
if nargin>=2
    if length(xlab)~=numX
        error('Wrong amount of X labels given');
    end
end

% Calculate the positions of the boxes
positions=1:0.25:numS*numX*0.25+1+0.25*numX;
positions(1:numS+1:end)=[];
boxWidth = 0.25;

% Extract data and label it in the group correctly
xx=[];
group=[];
for ii=1:numX
  for jj=1:numS
    aux=data{ii,jj};
    xx=vertcat(xx,aux(:));
    group=vertcat(group,ones(size(aux(:)))*jj+(ii-1)*numS);
  end
end
% Plot it

boxplot(xx,group, 'positions',positions, 'widths',boxWidth);
% boxplot(x,group, 'positions',positions, 'widths',0.25, 'PlotStyle','compact');
bak_holdState = ishold;
hold on

% % iosr.statistics.boxPlot(x,group, 'positions',positions, 'widths',0.25);
% bp = iosr.statistics.boxPlot( reshape(x,20,[]) );
% bp.scatterMarker = '.';
% bp.scatterSize = 80;
% bp.scatterColor = 'g';
% % bp.scatterColor = 'k'
% bp.showScatter = true;
% bp.showMean = true;
% % bp.showLegend = true;

% notBoxPlot(x,group,jitter,style)
% notBoxPlot(x,group)

% Set the Xlabels
aux=reshape(positions,numS,[]);
labelpos = sum(aux,1)./numS;

set(gca,'xtick',labelpos)
if nargin>=2
    set(gca,'xticklabel',xlab);
else
    idx=1:numX;
    set(gca,'xticklabel',strsplit(num2str(idx),' '));
end
    

% Get some colors
if nargin>=4
    cmap=colors;
else
    cmap = hsv(numS);
    cmap=vertcat(cmap',ones(1,numS)*0.2);
end
color=repmat(cmap, 1, numX);

% Apply colors
h = findobj(gca,'Tag','Box');
h = flipud(h); % reverse order: first to last
for jj=1:length(h)
   patch(get(h(jj),'XData'),get(h(jj),'YData'),color(1:3,jj)','FaceAlpha',color(4,jj));
end

% % Add jittered data
% generate random jitter
% kk = 1;
% for ii=1:numX
%   for jj=1:numS
%     boxData = data{ii,jj};
%     J = (rand(size(boxData))-0.5)*boxWidth;
%     plot( positions(kk)+J, boxData, '.', 'Color',color(1:3,jj) )
%     % update linear index
%     kk=kk+1;
%   end
% end

% % Add ordered data
% kk = 1;
% for ii=1:numX
%   for jj=1:numS
%     boxData = data{ii,jj};
%     J = linspace(-0.5,+0.5,numel(boxData))*boxWidth;
%     plot( positions(kk)+J, sort(boxData), '.', 'Color',color(1:3,jj) )
%     % update linear index
%     kk=kk+1;
%   end
% end

% Add points in median for better visualization
h=findobj(gca,'tag','Median'); % Get handles for median lines.
h = flipud(h); % reverse order: first to last
% Get central coordinates for each line
x = mean( cell2mat( {h.XData}' ), 2 );
y = mean( cell2mat( {h.YData}' ), 2 );
% Arrange point coordinates into different S-groups
x = reshape(x,numS,[]);
y = reshape(y,numS,[]);
for i=1:numS
  % Plot points for each group in different color
%   plot(x(i,:),y(i,:),'.','MarkerSize',15,'Color',color(1:3,i))
  plot(x(i,:),y(i,:),'o','Color',color(1:3,i),'MarkerFaceColor','w')
end

% Remove outliers
h=findobj(gca,'tag','Outliers');
delete(h)
ylim('auto')

% Add legend
if nargin>=3 && isnonempty(Mlab)
%   legend(Mlab,'Location','best');
end

% J=(rand(size(thisX))-0.5)*jitter;
% 
% h(k).data=plot(thisX+J, thisY, 'o', 'color', C,...
%   'markerfacecolor', C+(1-C)*0.65);

if ~bak_holdState
  hold off % toggle to off
end
end