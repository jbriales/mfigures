function setblktags(dim, sizes, tags, extratags)
% setblktags(dim, sizes, tags, extratags)
% Add tags block-wise

% Choose properties to modify according to dimension
switch dim
  case 1
    tick = 'XTick';
    tickLabel = 'XTickLabel';
  case 2
    tick = 'YTick';
    tickLabel = 'YTickLabel';
end

nBlks = numel(sizes);

% Get skip values from cumulative block dimensions
v  = [0 cumsum(sizes)];
dv = diff(v);
% Get numeric labels
if ~exist('tags','var') || isempty(tags)
  tags = cell(1,nBlks);
  for i=1:nBlks
    if v(i)+1 < v(i+1)
      currTag = sprintf('%d:%d',v(i)+1,v(i+1));
    else
      currTag = sprintf('%d',v(i+1));
    end
    tags{i} = currTag;
  end
end
v = v + 0.5; % Offset position to center better
tickPos = v(1:end-1) + dv/2;
% Set numeric ticks in the middle of blocks
set(gca,tick,tickPos);
set(gca,tickLabel,tags);
set(gca,'TickDir','out');

if exist('extratags','var')
  % Create new axes with same limits and same parameters as for spy
  ax = axis;
  axes('xlim', ax(1:2), 'ylim', ax(3:4), 'color', 'none',...
       'ydir','reverse', 'GridLineStyle','none',...
       'plotboxaspectratio',[ax(2) ax(4) 1]);
  % Add auxiliar axes to add extra tags
  switch dim
    case 1
      set(gca,'YTick',[], 'XAxisLocation','top')
    case 2
      set(gca,'XTick',[], 'YAxisLocation','right');
  end 
  % Set extra tags
  set(gca,tick,tickPos);
  set(gca,tickLabel,extratags);
  set(gca,'TickDir','out');
end