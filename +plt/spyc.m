function hF = spyc(sM,cmap,pb, hF)

%SPYC Visualize sparsity pattern with color-coded scale.
%   SPYC(S) plots the color-coded sparsity pattern of the matrix S.
%
%   SPYC(S,CMAP) plots the sparsity pattern of the matrix S USING 
%                    COLORMAP CMAP.
%
%   SPYC(S,CMAP,PB) allows turning off the display of a colorbar by passing
%                   flag PB=0
%
%   written by Try Hard
%   $Revision: 0.0.0.2 $  $Date: 2013/08/24 11:11:11 $

if nargin<1 | nargin>4 && ~isempty(cmap)
    error( 'spyc:InvalidNumArg', 'spyc takes one to three inputs')
    return
end

if isempty(sM)
    error( 'spyc:InvalidArg', 'sparse matrix is empty')
    return
end

if nargin>1 && ~isempty(cmap)
    % colorspy does not check whether your colormap is valid!
    if ~isnumeric(cmap)
        cmap=colormap(cmap);        
    end
else
    cmap=flipud(colormap('autumn'));
end

if nargin<3 || isempty(pb)
    pb=1;
end

% Get list of linear indexes for elements in sparse matrix sM
idxs=find(sM);
% Get list of xy positions of these indexes
[i j] = ind2sub(size(sM),idxs);
% Get list of non-zero values in sM
m = full(sM(idxs));

% Compute color values (point value) to use in scatter
% c = round((m-min(m))*63/(max(m)-min(m)))+1;
c = m;

if nargin < 4
  hF = figure;
end
hold on;
colormap(cmap)
x = j; y = i; % note the swap: x is columns (j), y is rows (i)
scatter(x,y,[],c,'filled','Marker','s','SizeData',150)


%% Set plot style and axes
%% ------------------------
% Reverse y (to point downwards, as usual for matrix rows)
set(gca,'ydir','reverse')
axis equal;
xlabel(['nz = ' num2str(numel(m))])

% Move axis to matrix index limits
[Nx,Ny] = size(sM);
% axis([0 Nx 0 Ny])
axis([0 Ny 0 Nx])

% Adjust tighter axis
% centeredAxis = axis;
% centeredAxis([1 3]) = centeredAxis([1 3]) + .5;
% centeredAxis([2 4]) = centeredAxis([2 4]) + .5;
centeredAxis = axis + .5;
axis(centeredAxis);
box on

if pb
  % Display plot colorbar
  colorbar
end
