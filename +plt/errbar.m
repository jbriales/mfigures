function hLeg = errbar(data,colors)
% hLeg = errbar(data,colors)
% 
% Data is a cell array (in the parameter space)
% Dim 1 (rows) is the S (grouping, or series) dimension (for analogy with stacking)
% Dim 2 (columns) is the X dimension (for analogy with horizontal axis)
% Each cell contains an array with N data (the data in the box)
% 
%            { [...], [...], [...]    |
%   INPUT      [...], [...], [...]    |  S dim (series)
%   FORMAT     [...], [...], [...] }  v
%    
%             ------- X dim ------>
% 
% 
% The implementation follows a progressive pipeline,
% from raw data to esthetics (following Making Pretty Graphs by Loren)

% check inputs
assert(iscell(data),'Input must be cell array, check doc')

% Get sizes
dims = size(data);
numS = dims(1);
numX = dims(2);

% Arrange data into columns (for concatenation in same series)
data = cellfun(@vec,data,'UniformOutput',false);
% Hold figure for accumulatin next elements
bak_holdState = ishold;
hold on

  function errUL = errbarfun(Y)
    p75 = prctile(Y,75,1);
    m   = median(Y,1);
    p25 = prctile(Y,25,1);
    errUL = [ p75-m ;
              m-p25 ];
  end

% h = gobjects(1,3); % Is struct, no gobject
for i=1:numS
  % For each series
  % get data (N observations by X cases)
  sdata = cell2mat(data(i,:));
  % plot
  format = {'Color',colors{i}};
%   h(i) = shadedErrorBar([],sdata,{@mean,@std},format,true);
%   h(i) = shadedErrorBar([],sdata,{@median,@std},format,true);
  h(i) = shadedErrorBar([],sdata,{@median,@errbarfun},format,true);
%   shadedErrorBar(x,mean(y,1),std(y),'g');
%   shadedErrorBar(x,y,{@median,@std},{'r-o','markerfacecolor','r'}); 
end

% Set X ticks in standard positions
r = get(gca,'XRuler');
r.TickValues = 1:numX;
% keyboard

% % TEMP
% a = gca;
% a.YScale = 'log';

% keyboard

% % Get statistical function of the data (bar can only plot scalar)
% % statfun = @mean;
% statfun = @(x)mean(x)*100;
% statdata = cellfun( statfun, data );
% 
% % Prepare data for native Matlab bar
% % NOTE: bar(X,Y) assumes Y has group idx as rows, series idx as cols
% %       We transpose it to meet our criteria (see doc)
% barWidth = 1;
% hBar = bar(mean(positions,1), statdata', barWidth);
% 
% % Set the X ticks
% set(gca,'xtick',mean(positions,1)); % mean accross series positions  
% 
% %% Adjust Line Properties (Functional)
% % List of handles: hBox, hPatch, hMed, hMedMark, hOut, hSorted
% % Set colors
% for i=1:numS
%   % Bar faces
%   set(hBar(i), 'FaceColor', colors{i});
% end
% 
% %% Adjust Line Properties (Esthetics)
% % Anything?

%% Add legend
[hLeg,OBJH,OUTH,OUTM] = legend( [h.patch] );

if ~bak_holdState
  hold off % toggle to off
end

end

function h = gethandles(type,size)
% h = gethandles(TYPE)
% Get an array of the same size as the parameter space, numS x numX
% with the handles of the boxplot objects tagged as TYPE

h = findobj(gca,'tag',type); % Get handles for median lines.
h = flipud(h); % reverse order: first to last (in boxplot it works!?)
h = reshape( h, size );

assert(numel(h)==prod(size),'Check the boxplot is in an empty axis')
end