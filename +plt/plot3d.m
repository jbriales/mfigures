function h = plot3d( m, varargin )

c_m = num2cell(m,2);
h = plot3( c_m{:}, varargin{:} );

end