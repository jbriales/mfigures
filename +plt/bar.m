function hLeg = bar(data,colors)
% hLeg = bar(data,colors)
% 
% Data is a cell array (in the parameter space)
% Dim 1 (rows) is the S (grouping, or series) dimension (for analogy with stacking)
% Dim 2 (columns) is the X dimension (for analogy with horizontal axis)
% Each cell contains an array with N data (the data in the box)
% 
%            { [...], [...], [...]    |
%   INPUT      [...], [...], [...]    |  S dim (series)
%   FORMAT     [...], [...], [...] }  v
%    
%             ------- X dim ------>
% 
% 
% The implementation follows a progressive pipeline,
% from raw data to esthetics (following Making Pretty Graphs by Loren)

% check inputs
assert(iscell(data),'Input must be cell array, check doc')

% Get sizes
dims = size(data);
numS = dims(1);
numX = dims(2);

% Get statistical function of the data (bar can only plot scalar)
% statfun = @mean;
statfun = @(x)mean(x)*100;
statdata = cellfun( statfun, data );

% Calculate the positions of the boxes
boxWidth = 0.25; % box width
% positions=1 : 0.25 : numS*numX*0.25+1+0.25*numX;
positions=1 : boxWidth : (numS+1)*numX*boxWidth+1;
% positions=0 : boxWidth : (numS+1)*numX*boxWidth;
positions(1:numS+1:end)=[]; % drop gap boxes
positions = reshape( positions, numS,numX );

% Prepare data for native Matlab bar
% NOTE: bar(X,Y) assumes Y has group idx as rows, series idx as cols
%       We transpose it to meet our criteria (see doc)
barWidth = 1;
if isvector(positions)
  % ugly hack, hate you Matlab
  doHack = true;
  positions = [positions,positions+1];
  statdata  = [statdata, statdata ];
end
hBar = bar(mean(positions,1), statdata', barWidth);
if exist('doHack','var') && doHack==true
  % undo hack (extra group)
  for i=1:numS
    % Remove extra bar (hacking bar)
    hBar(i).XData(end) = [];
  end
end  

% Set the X ticks
set(gca,'xtick',mean(positions,1)); % mean accross series positions  

%% Adjust Line Properties (Functional)
% List of handles: hBox, hPatch, hMed, hMedMark, hOut, hSorted
% Set colors
for i=1:numS
  % Bar faces
  set(hBar(i), 'FaceColor', colors{i});
end

%% Adjust Line Properties (Esthetics)
% Anything?

%% Add legend
lim = xlim(); % legend is breakin limits, save and restore
[hLeg,OBJH,OUTH,OUTM] = legend( hBar, 'Location', 'bestoutside' );
% hLeg.Location = 'bestoutside';
xlim(lim);

% TEMP: Make Y lim bigger than 100 for better visibility
ylim([0 105])
hRefline = refline(0,100);
hRefline.Color = 'k';
hRefline.LineStyle = '--';

end

function h = gethandles(type,size)
% h = gethandles(TYPE)
% Get an array of the same size as the parameter space, numS x numX
% with the handles of the boxplot objects tagged as TYPE

h = findobj(gca,'tag',type); % Get handles for median lines.
h = flipud(h); % reverse order: first to last (in boxplot it works!?)
h = reshape( h, size );

assert(numel(h)==prod(size),'Check the boxplot is in an empty axis')
end