function saveTex( name, hF,format )

if ~exist('hF','var')
  hF = gcf;
end
if ~exist('format','var')
  format = '-depsc';
end

fig_dir  = getenv('DIR_TEX_FIG');
if isempty(fig_dir)
  error('Please set the path for saving Latex figures: setenv(''DIR_TEX_FIG'',...)');
end
fig_file = fullfile(fig_dir,name);
[parent_folder,~,~] = fileparts(fig_file);
if ~exist(parent_folder,'dir')
  mkdir(parent_folder);
end
print( hF, format, fig_file ) % -depsc
end