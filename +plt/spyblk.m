function spyblk(rsizes, csizes, xtags, ytags)
%SPYBLK Add block partitions to spy plot.
%   SPYBLK(rp,cp) plots lines marking a block partition described by
%   rp (rows) and cp (columns).
%   If S is square, cp may be omitted and defaults to rp.

if (nargin < 2)
   csizes = rsizes;
end

% Get axis limits
ax = axis;

% Save figure hold state and activate hold
previousHold = ishold;
if ~previousHold
  hold on
end
  
% Plot horizontal lines (cnst Y)
nBlks = numel(rsizes);
if nBlks > 1
  % Get X values of line extreme points from axis limits
  X = ax(1:2);
  X = repmat(X(:),1,nBlks-1);
  
  % Get Y values from cumulative block dimensions
  Y  = [0 cumsum(rsizes)];
  dY = diff(Y);
  % Get labels
  if ~exist('xtags','var') || isempty(xtags)
  xtags = cell(1,nBlks);
  for i=1:nBlks
    if Y(i)+1 < Y(i+1)
      currTag = sprintf('%d:%d',Y(i)+1,Y(i+1));
    else
      currTag = sprintf('%d',Y(i+1));
    end
    xtags{i} = currTag;
  end
  end
  Y = Y + 0.5; % Offset position to center better
  % Set ticks in the middle of blocks
  set(gca,'YTick',Y(1:end-1) + dY/2)
  set(gca,'YTickLabel',xtags);
  
  % Skip outer borders to plot
  Y = Y(2:end-1);
  % Repeat Y for start and end lines
  Y = repmat(Y,2,1);
  
  % Plot block lines
  plot(X,Y,'k-')
end

% Plot vertical lines (cnst X)
nBlks = numel(csizes);
if nBlks > 1
  % Get X values of line extreme points from axis limits
  Y = ax(3:4);
  Y = repmat(Y(:),1,nBlks-1);
  
  % Get X values from cumulative block dimensions
  X = [0 cumsum(csizes)];
  % Get labels
  ytags = cell(1,nBlks);
  for i=1:nBlks
    ytags{i} = sprintf('%d:%d',X(i)+1,X(i+1));
  end
  X = X + 0.5; % Offset position to center better
  % Set ticks in the middle of blocks
  set(gca,'XTick',X(1:end-1) + diff(X)/2)
  set(gca,'XTickLabel',ytags);
  
  % Skip outer borders to plot
  X = X(2:end-1);
  % Repeat Y for start and end lines
  X = repmat(X,2,1);
  
  % Plot block lines
  plot(X,Y,'k-')
end
% axis ij

% Return to original hold state
if ~previousHold
  hold off
end

end

function plotlines(dim,sizes,limits)
% plotlines(dim,sizes,limits)
% Plot lines parallel to axis dim

% Assume we are plotting horizontal lines (dim=1, parallel to X):
%   X between borders
%   List of cnst Y
% If dim=2, we will permute X and Y later

nBlks = numel(sizes);
if nBlks > 1
  % Get X values of line extreme points from axis limits
  X = repmat(limits(:),1,nBlks-1);
  
  % Get Y values from cumulative block dimensions
  Y  = [0 cumsum(sizes)];
  dY = diff(Y);
  % Get labels
  tags = cell(1,nBlks);
  for i=1:nBlks
    if Y(i)+1 < Y(i+1)
      currTag = sprintf('%d:%d',Y(i)+1,Y(i+1));
    else
      currTag = sprintf('%d',Y(i+1));
    end
    tags{i} = currTag;
  end
  Y = Y + 0.5; % Offset position to center better
  tickPos = Y(1:end-1) + dY/2;
  
  % Skip outer borders to plot
  Y = Y(2:end-1);
  % Repeat Y for start and end lines
  Y = repmat(Y,2,1);
  
  % Plot block lines
  switch dim
    case 1
      plot(X,Y,'k-')
      tick = 'YTick';
      tickLabel = 'YTickLabel';
    case 2
      plot(Y,X,'k-')
      tick = 'XTick';
      tickLabel = 'XTickLabel';
  end
  % Set ticks in the middle of blocks
  set(gca,tick,tickPos);
  set(gca,tickLabel,tags);
end
end