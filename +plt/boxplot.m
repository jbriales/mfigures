function hLeg = boxplot(data,colors, outthres)
% hLeg = boxplot(data,colors)
% 
% Data is a cell array (in the parameter space)
% Dim 1 (rows) is the S (grouping, or series) dimension (for analogy with stacking)
% Dim 2 (columns) is the X dimension (for analogy with horizontal axis)
% Each cell contains an array with N data (the data in the box)
% 
%            { [...], [...], [...]    |
%   INPUT      [...], [...], [...]    |  S dim (series)
%   FORMAT     [...], [...], [...] }  v
%    
%             ------- X dim ------>
% 
% 
% The implementation follows a progressive pipeline,
% from raw data to esthetics (following Making Pretty Graphs by Loren)

%
% Optional:
% xlab is a cell array of strings of length L with the names of each
% group
%
% Mlab is a cell array of strings of length M
%
% colors is a Mx4 matrix with normalized RGBA colors for each M.

% check inputs
assert(iscell(data),'Input must be cell array, check doc')

% Get sizes
dims = size(data);
numS = dims(1);
numX = dims(2);

% Calculate the positions of the boxes
boxWidth = 0.25; % box width
% positions=1 : 0.25 : numS*numX*0.25+1+0.25*numX;
positions=1 : boxWidth : (numS+1)*numX*boxWidth+1;
positions(1:numS+1:end)=[]; % drop gap boxes
positions = reshape( positions, numS,numX );

% Prepare data for native Matlab boxplot
% For increased flexibility, use single stacked vector with grouping var.
% This allows for e.g. different number of samples in each group.
% Force columns in data cells

% NOTE: We have commented THIS line below! Careful
% data = cellfun(@(x)x(:),data,'UniformOutput',false);

% Create group mask
data_group = cell(size(data));
% traverse cell array column-wise (series first)
for i=1:numS
  for j=1:numX
    groupIdx = sub2ind(size(data_group),i,j);
    data_group{i,j} = groupIdx * ones(size(data{i,j}));
  end
end
% Call native Matlab boxplot
vec_data  = vec(cell2mat(data));
vec_group = vec(cell2mat(data_group));
boxplot(vec_data,vec_group, 'positions',vec(positions), 'widths',boxWidth);

% Hold figure for adding next elements
bak_holdState = ishold;
hold on

% Set the X ticks
set(gca,'xtick',mean(positions,1)); % mean accross series positions  

%% Create patches for coloring the boxes
% NOTE: Find all possible tags in More About section of boxplot doc
hBox = gethandles('Box',dims);
hPatch = hBox;
for ii=1:numel(hBox)
%    patch(get(h(jj),'XData'),get(h(jj),'YData'),color(1:3,jj)','FaceAlpha',color(4,jj));
  hPatch(ii) = patch('XData',hBox(ii).XData,...
                     'YData',hBox(ii).YData);
end

%% Modify median for better visualization
hMed = gethandles('Median',dims);
hMedMark = hMed;
for ii=1:numel(hMed)
  % Add mark in the middle of the median line (better visualization)
  hMedMark(ii) = plot( mean(hMed(ii).XData), mean(hMed(ii).YData), 'x' );
end
% Put together
hMed = repmat(hMed,1,1,2);
hMed(:,:,2) = hMedMark; % 1st layer: hor segment, 2nd layer: mark

%% Get outliers
hOut = gethandles('Outliers',dims);
% delete(hOut) % Choose to do later?

%% Plot sorted data within each box
hSorted = gobjects(dims);
for i=1:numS
  for j=1:numX
    boxData = data{i,j};
    % Filter outliers (TODO: Make an option)
    % Get outlier values
    outVals = hOut(i,j).YData;
    % Keep only upper outliers (above median)
    outVals(outVals<median(boxData))=[];
    % Quick hack for upper outliers only: Seek minimum and filter
    if ~isempty(outVals)
      boxData( boxData >= min(outVals) ) = [];
    end
    % Clear data to avoid clustering?
    % Get relative X positions within the box
    J = linspace(-0.5,+0.5,numel(boxData))*boxWidth; % sorted jitter
    % J = (rand(size(boxData))-0.5)*boxWidth; % random jitter
    % Plot data
    hSorted(i,j) = plot( positions(i,j)+J, sort(boxData), '.' );
  end
end

%% Adjust Line Properties (Functional)
% List of handles: hBox, hPatch, hMed, hMedMark, hOut, hSorted
% Set colors
for i=1:numS
  % Line features
  set([hMed(i,:), hMedMark(i,:), hSorted(i,:)], ...
     'Color'           , colors{i}   );
  % Patches
  set(hPatch(i,:),...
     'FaceColor'       , colors{i}   , ...
     'FaceAlpha'       , 0.2         );
end

% Outliers
% Do not remove for CVPR18 experiments
% delete(hOut) % Remove

% Set a threshold as to what is an outlier
% This makes most sense when the quantity is >0
for i=1:size(hOut,1)
  for j=1:size(hOut,2)
    l = hOut(i,j);
    % Find indeces of mild outliers (<tol)
    smallerThanTol = l.YData < outthres;
    l.XData(smallerThanTol) = [];
    l.YData(smallerThanTol) = [];
    
    % Set outlier color
    l.Color = colors{i};
    l.MarkerEdgeColor = colors{i};
    % Set outlier marker style
%     l.Marker = '.';
    l.Marker = 'o';
    
%     drawnow
%     keyboard
  end
end

%% Adjust Line Properties (Esthetics)

% Set maximum Y according to where most of the data is? Prctile?
% TODO: Set externally?
% ylimmin( 0 )
% ylimmax( prctile(vec_data,90) )
ylim('auto')

%% Add legend
% v_leghandles = [hPatch(:,1), hMedMark(:,1)];
v_leghandles = hPatch(:,1);
[hLeg,OBJH,OUTH,OUTM] = legend( vec(v_leghandles) );

if ~bak_holdState
  hold off % toggle to off
end
end

function h = gethandles(type,size)
% h = gethandles(TYPE)
% Get an array of the same size as the parameter space, numS x numX
% with the handles of the boxplot objects tagged as TYPE

h = findobj(gca,'tag',type); % Get handles for median lines.
h = flipud(h); % reverse order: first to last (in boxplot it works!?)
h = reshape( h, size );

assert(numel(h)==prod(size),'Check the boxplot is in an empty axis')
end